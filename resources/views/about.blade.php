<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="GeTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Miniml - Portfolio HTML Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">

    <!-- Font Awesome Icons CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">

    <!-- Magnific-popup CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">

    <!-- Google Fonts Link -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" type="text/css">
</head>
<body>
    <!-- PRE LOADER -->
    <div class="preloader">
        <div class="sk-spinner sk-spinner-pulse"></div>
    </div>

    <!-- Navigation Section -->
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>
                <a href="index.html" class="navbar-brand">MINIML.</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="/about">About</a></li>
                    
                    <li><a href="/projects">Work</a></li>
                 
                </ul>
            </div>
        </div>
    </div>

    <!-- Home Section -->
    <section id="home" class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h1 class="wow fadeInUp head" data-wow-delay="0.6s">My Work exprience</h1>
                    <p class="wow fadeInUp lr-pd" data-wow-delay="0.8s">Leading Digital Agency with deep creative insight based in California. We help brands and businesses build communication across Web, Print, and Digital Medium. Let's work together.</p>
                </div>
            </div>
        </div>
    </section>
        <!-- Work Experience Timeline Section -->
            <section style="background-color: #F0F2F5;">
                <div class="container py-5">
                    @foreach ( $data as $key => $value )
                        
                
                    <div class="main-timeline">
                            @if ( $key % 2 == 0 )
                                
                           
                            <div class="timeline left">
                                <div class="card">
                                <div class="card-body p-4">
                                    <h3>{{ $value['time'] }}</h3>
                                    <p><b>Position:</b> {{ $value['position'] }}</p>
                                    <p class="mb-0">{{ $value['description'] }}</p>
                                    <p><b>Company:</b> {{ $value['company'] }}</p>
                                    <p><b>Location:</b> {{ $value['location'] }}</p>
                                </div>
                                </div>
                            </div>
                            @else
                            <div class="timeline right">
                                <div class="card">
                                    <div class="card-body p-4">
                                        <h3>{{ $value['time'] }}</h3>
                                        <p><b>Position:</b> {{ $value['position'] }}</p>
                                        <p class="mb-0">{{ $value['description'] }}</p>
                                        <p><b>Company:</b> {{ $value['company'] }}</p>
                                        <p><b>Location:</b> {{ $value['location'] }}</p>
                                    </div>
                                </div>
                            </div>
                            @endif    
              
                    </div>

                    @endforeach
                </div>
            </section>
        <!-- End of Work Experience Timeline Section -->


       

  


      


        <!-- Footer Section -->

        <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="footer-copyright">
                        <ul class="social-icon">
                            <li><a href="#" class="fa fa-facebook wow fadeInUp" data-wow-delay="0.2s"></a></li>
                            <li><a href="#" class="fa fa-twitter wow fadeInUp" data-wow-delay="0.4s"></a></li>
                            <li><a href="#" class="fa fa-linkedin wow fadeInUp" data-wow-delay="0.6s"></a></li>
                            <li><a href="#" class="fa fa-google-plus wow fadeInUp" data-wow-delay="0.8s"></a></li>
                            <li><a href="#" class="fa fa-dribbble wow fadeInUp" data-wow-delay="1s"></a></li>
                        </ul>
                        <p class="small">&copy; Copyright 2018 Miniml HTML Template - All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Back to top -->
    <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

    <!-- SCRIPTS -->
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/isotope.js') }}"></script>
    <script src="{{ asset('assets/js/imagesloaded.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/magnific-popup-options.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>
