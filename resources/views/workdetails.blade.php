<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="GeTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Miniml - Portfolio HTML Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">

    <!-- Font Awesome Icons CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">

    <!-- Magnific-popup CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">

    <!-- Google Fonts Link -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" type="text/css">
</head>
<body>
    <!-- PRE LOADER -->
    <div class="preloader">
        <div class="sk-spinner sk-spinner-pulse"></div>
    </div>

    <!-- Navigation Section -->
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>
                <a href="index.html" class="navbar-brand">MINIML.</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="/about">About</a></li>
                    
                    <li><a href="/projects">Work</a></li>
                 
                </ul>
            </div>
        </div>
    </div>

    <!-- Home Section -->
    <section id="home" class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h1 class="wow fadeInUp head" data-wow-delay="0.6s">{{ $projectdata['project_title'] }}</h1>
                   
                </div>
            </div>
        </div>
    </section>

        <!-- Proejcts Section -->

        <section id="service">
    <div class="container">
        <div class="row">
            <div class="text-center"> 
                <div class="wow fadeInUp col-md-4 col-sm-6" data-wow-delay="0.4s">
                    <div class="bg-white">
                        <img src="{{ asset($projectdata['image_link']) }}" alt="Project Image" class="img-responsive text-center" style="width: 600px; height: 600px;">
                        <p class="small">{{ $projectdata['project_details'] }}</p>
                        <span><b>Used Techonology: </b>{{ $projectdata['used_technology'] }}</span>
                        <span><b>Production date: </b>{{ $projectdata['date'] }}</span>
                    </div>
                </div>

                <!-- Add similar code for the remaining items -->
            </div>
        </div>
    </div>
</section>




        <!-- Footer Section -->
        <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="footer-copyright">
                        <ul class="social-icon">
                            <li><a href="#" class="fa fa-facebook wow fadeInUp" data-wow-delay="0.2s"></a></li>
                            <li><a href="#" class="fa fa-twitter wow fadeInUp" data-wow-delay="0.4s"></a></li>
                            <li><a href="#" class="fa fa-linkedin wow fadeInUp" data-wow-delay="0.6s"></a></li>
                            <li><a href="#" class="fa fa-google-plus wow fadeInUp" data-wow-delay="0.8s"></a></li>
                            <li><a href="#" class="fa fa-dribbble wow fadeInUp" data-wow-delay="1s"></a></li>
                        </ul>
                        <p class="small">&copy; Copyright 2018 Miniml HTML Template - All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Back to top -->
    <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

    <!-- SCRIPTS -->
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/isotope.js') }}"></script>
    <script src="{{ asset('assets/js/imagesloaded.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/magnific-popup-options.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>
