<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    public function index(){
        $jsonFile = storage_path("app/work-exprince.json");

        if( file_exists($jsonFile) ){
            $jsonContent = file_get_contents($jsonFile);
            $data = json_decode($jsonContent, true);
            return view("about", ["data"=> $data]);
        }else{
            return view("about", ["data"=> []]);
        }

        
        
    }
}
