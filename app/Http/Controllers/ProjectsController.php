<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(){
        $jsonFile = storage_path("app/projects.json");
        if( file_exists($jsonFile) ){
            $jsonContent = file_get_contents($jsonFile);
            $data = json_decode($jsonContent, true);
            return view("work", ["projects"=> $data]);
        }else{
            return view("work", ["projetcs"=> []]);
        }
        
    }

    public function show($id) {


        $jsonFile = storage_path("app/projects.json");
        if( file_exists($jsonFile) ){
            $jsonContent = file_get_contents($jsonFile);
            $data = json_decode($jsonContent, true);
            $data = collect($data)->firstWhere( 'id', $id );
            return view("workdetails", ["projectdata"=> $data]);
        }
    }
    


}
